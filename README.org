#+TITLE: Projet de principes de programmation : un client Gemini
#+AUTHOR: Joseph Le Roux, Nicolas Munnich, Pierre Boudes Paris 13
#+EMAIL: 
#+LANGUAGE:  fr
#+MACRO: paraphe L3 Info
#+EXPORT_SELECT_TAGS: export
#+EXPORT_EXCLUDE_TAGS: noexport

# # REVEALJS
# #+INCLUDE: includes/revealpresentation.org

#+OPTIONS: reveal_width:1280 reveal_height:900
# +OPTIONS: reveal_width:1000 reveal_height:600

# LATEX
#+INCLUDE: includes/latexuspnm2pls.org
#+LATEX_HEADER: \usepackage{tabularx}

# HTML
#+INCLUDE: includes/html.org

{{{bititre(Projet de principes de programmation :, un client Gemini)}}}


#+BEGIN_EXPORT latex
\begin{center}
\ainstaller[0.08]{../img/logo-ocaml.png}{https://ocaml.org}{ocaml}
%\ainstaller[0.02]{../img/logo-scala.png}{https://scala-lang.org}{scala}
%\ainstaller[0.1]{../img/logo_sbt.png}{http://www.scala-sbt.org/}{sbt}
%\ainstaller[0.02]{../img/logo-scala-js.png}{https://scala-js.org}{scala-js}
%\ainstaller[0.13]{../img/logo-ensime2.png}{http://ensime.github.io}{ensime}
%\ainstaller[0.27]{../img/lihaoyi.png}{http://ammonite.io/#Ammonite-REPL}{ammonite}
% \ainstaller[0.07]{../img/logo_akka.png}{http://akka.io}{akka}
% \ainstaller[0.35]{../img/logo-emacs.png}{//https://www.gnu.org/software/emacs/}{emacs}
% \ainstaller[0.09]{../img/logo_jquery.png}{https://jquery.com/}{jQuery}}
\end{center}
#+END_EXPORT


* Introduction

[[https://en.wikipedia.org/wiki/Gemini_(protocol)][Gemini]] est un nouveau protocole internet, plus léger que HTTP, apparu en juin 2019. Comme le web, il permet de servir des pages dans un format hypertexte mais celui-ci est quasiment réduit à l'essentiel : du texte et des liens et un peu de markdown pour bien présenter l'information. Il y a actuellement (25 mars 2021) environ 2000 capsules (équivalent dans Gemini à un site web).

Une URI Gemini se reconnaît car elle commence par ~Gemini://~ au lieu de ~https://~. Tout le traffic Gemini est chiffré avec TLS. Il n'y a donc pas d'équivalent de http où la communication réseau se fait sans chiffrement. 

Votre travail de projet consiste en la programmation d'un client texte (dans le terminal) pour Gemini, en OCaml. Ce client utilisera la bibliothèque lwt pour effectuer les requêtes réseau asynchrone. Vous devrez également trouver au moins une façon d'utiliser un zipper et indiquer à quel endroit dans votre documentation (le README). 

* Organisation

Vous devez suivre une organisation précise basée sur le [[https://gitlab.sorbonne-paris-nord.fr/][Gitlab de l'université]]. Il faut y accéder au moins une fois, pour que votre compte soit initialisé et que nous puissions vous donner des droits. Si vous avez déjà accédé au à gitlab.sorbonne-paris-nord.org ou à chat.sorbonne-paris-nord.org (le Mattermost) c'est bon. 

Vous devez  commencer par former un binôme composé d'exactement deux personnes. Vous pouvez former des binômes entre deux groupes mais vous ne serez suivi par un enseignant de l'un des deux groupes.

** Votre dépôt git sur le Gitlab

Vous devez ensuite créer un dépot *privé* sur ce Gitlab vous pourrez toujours changer de serveur /remote/ une fois le projet terminé. Vous pouvez choisir librement son nom mais nous vous recommandons d'utiliser au moins le texte "Gemini-client" dans celui-ci. Ses membres doivent être les deux membres de votre binôme et les trois enseignants dont les identifiants sont =joseph.leroux=, =nicolas.munnich= et =boudes= avec un rôle /Maintainer/. 

Vous devez chacun et chacune avoir créé votre paire de clé ssh et avoir déposé la clé publique sur le serveur Gitlab (dans votre [[https://gitlab.sorbonne-paris-nord.fr/-/profile/keys][profil]]). Vous pouvez définir plusieurs clés ssh. nous vous le recommandons en particulier lorsque vous utilisez plusieurs postes de travail, par exemple votre ordinateur personnel et votre compte en salle TP. 

Tout votre code source devra être déposé en utilisant git dans votre dépôt. Et uniquement le code source. Votre dépôt ne doit pas dépasser 1Mo de stockage, réfléchissez bien à ce que vous commitez. Nous lirons votre historique de commits pour comprendre votre progression, essayez de faire des messages courts et clairs.

Pour démarrer avec git, nous vous rappelons quelques commandes de base :

#+ATTR_LATEX: :environment tabularx  :align | p{5cm} X| :width 0.999\linewidth
| Commandes                    | Signification                                                       |
|------------------------------+---------------------------------------------------------------------|
| =git checkout url=           | une seule fois pour récupérer le dépot                              |
| =git pull=                   | modifier le dépot local avec la version actuellement sur le serveur |
| =git add fichier=            | pour ajouter un fichier dans son état actuel au prochain commit     |
| =git commit -m "message"=    | pour effectuer un commit avec le message donné                      |
| =git push=                   | pousser sur le serveur vos modifications locales                    |
| =git add -u=                 | ajouter toutes les modifications de ficihiers déjà dans le dépot    |
| +git add -a+                 | anti-pattern ne jamais l'utiliser !                                 |
|------------------------------+---------------------------------------------------------------------|
 
La puissance de git se révèle quand on utilise les branches mais nous n'entrons pas dans le détail pour le moment. 

Commencez par écrire un =README.md= (ou =README.org= si vous aimez Emacs), dans lequel vous raconterez à quelle étape vous en êtes de votre projet. 

** Déclaration du binôme
 
En plus de créer votre dépôt git vous devez [[https://gitlab.sorbonne-paris-nord.fr/2022_pripro_projets/pripro2022suivi/-/issues/new?issue%5Bmilestone_id%5D=10][créer un ticket de déclaration du binôme]]. Donnez vos nom, vos groupes de TP, le lien vers votre dépôt et le lien vers votre fichier README.md ou README.org. Vous pouvez suivre l'exemple donné dans le [[https://gitlab.sorbonne-paris-nord.fr/2022_pripro_projets/pripro2022suivi/-/issues/1][ticket #1]].

Si vous n'avez pas accès aux tickets ci-dessus pas de panique ! Signalez vous (Mattermost, mail) en donnant votre numéro étudiant et nous vous ouvrions l'accès rapidement.

* Progression

** Première étape
Vous avez un binôme, un dépôt sur le gitlab de l'université et un ticket de suivi de votre travail et le 4 avril vous saurez quel enseignant vous suit. Vous aurez alors terminé la première étape.

** Dune
La seconde étape consiste en prendre en main l'environnement Dune pour le développement de projet en OCaml. Pour cela nous vous avons préparé un /template/ sous la forme d'un projet minimal, que vous pourrer copier comme point de départ de votre dépôt. 

Pour compiler tapez simplement la commande suivante.

#+begin_src sh
dune build
#+end_src

Vous pouvez installer les binaires compilés (y compris les bibliothèques) sur votre compte utilisateur, dans le répertoire =~/.opam= en tapant =dune install= (et désinstaller avec =dune uninstall=). Vous pouvez choisir de ne pas installer et juste tester votre programme en l'exécutant depuis le répertoire =_build= avec la commande =_build/default/bin/main.exe=.

** Gemini et design de votre client

Pour la troisième étape vous devez commencer à construire le design de votre client. Celui-ci doit fonctionner dans un terminal. Quelles seront les actions possibles de l'utilisatrice ? Quelles informations afficher ? Comment assurer la reprise sur erreur ?

N'hésitez pas à utiliser le système de tickets de gitlab sur votre dépôt pour prendre en note et assurer le suivi des fonctionnalités attendues. Par exemple si le la touche =esc= servira à quitter le programme, créez un ticket intitulé « quitter l'application » avec comme description « en tant qu'utilisateur / utilisatrice, je souhaite pouvoir quitter l'application en utilisant la touche =esc= ». Cela parait trivial mais quand on commence à avoir beaucoup de fonctionnalités cela permet de ne pas oublier quelque chose d'essentiel.

Avant cela nous vous invitions à découvrir un peu mieux Gemini, pour comprendre notamment comment s'effectue la navigation et vous permettre de vous inspirer de clients existants. 

Commencez par https://gemini.circumlunar.space. Vous y aurez une première expérience de ce qu'est une capsule Gemini (ici proposée par proxy en https), vous y trouverez une longue liste de clients et une description du protocole et de la variante de Markdown employée.

Ensuite il faut absolument que vous testiez des clients existants. Vous pouvez commencer avec un client démo en [[https://tildegit.org/solderpunk/gemini-demo-1][Python]] puis aller vers des clients plus évolués comme lagrange, AV-98 ou amfora. Plutôt que d'installer les clients vous pouvez en tester trois (Amfora, AV98 et Bombadillo) en utilisant ssh via un service /kiosk/. Ouvrez un terminal et connectez vous comme ceci :
~ssh kiosk@gemini.circumlunar.space~.

Un point obligatoire du design de votre client : vous devez faire un affichage qui tienne sur 80 colonnes la largeur standard d'un terminal. en particulier il faut introduire des sauts à la ligne automatiquement dans le texte des pages pour respecter cette limite.

** Requêtes réseau

Pour effectuer toutes vos requêtes réseau vous devez utiliser Lwt. Vous aurez besoin d'intégrer la cryptographie car le protocole Gemini ne fonctionne qu'avec TLS (/transport layer security/, le /s/ de https). Commencez par tester des requêtes réseau HTTP/1.1 avec Lwt. Vous pouvez par exemple vous inspirer de l'exemple fourni dans la [[https://github.com/ocsigen/lwt][documentation d'Ocsigen]]. 

Nous vous fournissons un code minimal pour effectuer une requête vers une capsule Gemini et récupérer le texte d'une page. Il suffit de sortir le second bloc de la liste passée à =Lwt.join= du main pour le tester.

** Affichage
Pour effectuer les affichages nous vous recommandons d'utiliser la bibliothèque [[ https://github.com/ocaml-community/lambda-term][lambda-term]]. Attention à ne pas passer trop de temps à chercher comment effectuer les affichages. Vous pouvez vous aider des exemples fournis par cette bibliothèque pour comprendre son fonctionnement. 

Rappelez-vous que nous espérons aussi trouver au moins une application des zippers dans votre code. Plusieurs ce serait encore mieux !

Maintenant que vous savez ce que vous pouvez faire avec lambda-term reprenez le design de votre application.  

** Améliorations

Vous pouvez penser à des amélioration de votre projet comme la possibilité de naviguer dans l'historique, la gestion de signets, l'ajout d'une gestion des onglets (liste non exhaustive). 

* Livrables
 
Le rendu de votre projet est votre dépôt git. Il doit contenir un =README= qui explique ce qui fonctionne et ce qui ne fonctionne pas ainsi que votre démarche. Pensez à expliquer où est-ce que vous utilisez des zippers. Vous devez également fournir le mode d'emploi de votre client, en particulier comment utiliser le clavier pour le défilement, la navigation dans les liens ou dans l'historique, la saisie d'une uri, la sauvegarde d'une page sous forme d'un fichier etc.

Ne copiez pas votre code ailleurs, sans le comprendre. En cas de doute de notre part sur l'originalité de votre travail nous vous interrogerons à l'oral.

Nous évaluerons votre travail *le 25 avril* à partir de 9h heure de Paris. Il faut donc que vous l'ayiez terminé avant.

Vous pourrez peut-être continuer à l'améliorer ensuite, mais il faudra le signaler sur votre ticket de déclaration de binôme et vérifier avec l'enseignant qui vous suivra de combien de temps vous disposez. Nous n'évaluerons pas les travaux au delà de la date du partiel, le 9 mai.

Nous sommes là pour vous aider !



